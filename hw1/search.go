package hw1

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"
)

func find(query string, rawUrls []string) ([]string, error) {

	if len(rawUrls) == 0 {
		return nil, errors.New("len urls is 0")
	}

	searchableUrls := make([]*url.URL, 0)
	for _, rawUrl := range rawUrls {
		parsedUrl, err := url.Parse(rawUrl)
		if err != nil {
			return nil, fmt.Errorf("unable to parse url %s : %w", rawUrl, err)
		}

		searchableUrls = append(searchableUrls, parsedUrl)
	}

	client := &http.Client{
		Transport: http.DefaultTransport,
		Timeout:   2 * time.Second,
	}

	wg := &sync.WaitGroup{}
	wg.Add(len(searchableUrls))

	result := make([]string, 0)
	for _, urlToSearch := range searchableUrls {
		go func(u *url.URL) {
			if url, err := search(client, query, u); err == nil && url != nil {
				result = append(result, url.String())
			}
			wg.Done()
		}(urlToSearch)
	}
	wg.Wait()

	return result, nil
}

func search(client *http.Client, query string, u *url.URL) (*url.URL, error) {

	var foundedUrl *url.URL
	rawUrl := u.String()
	resp, err := client.Get(rawUrl)
	if err != nil {
		return nil, fmt.Errorf("unable to get url [%s] due [%s]", rawUrl, err)
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unable to get url [%s] status code [%d] not equal 200", rawUrl, resp.StatusCode)
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("unable to read body due [%s]", err)
	}

	if strings.Contains(string(data), query) {
		foundedUrl = u
	}

	return foundedUrl, nil
}
