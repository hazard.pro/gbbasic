package hw1

import (
	"testing"
)

func TestSearch(t *testing.T) {
	query := "yandex"
	urls, err := find(query, []string{"https://yahoo.ru", "https://yandex.ru", "https://me.com", "https://google.com"})
	if err != nil {
		t.Errorf("unable to find query [%s] due [%s]", query, err)
	}

	if len(urls) != 1 {
		t.Errorf("wrong count of right urls")
	}

	if urls[0] != "https://yandex.ru" {
		t.Errorf("wrong host result")
	}
}
